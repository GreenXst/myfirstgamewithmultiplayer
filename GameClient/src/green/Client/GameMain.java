package green.Client;

public class GameMain {
    public static void main(String[] args){
        GameMain gameMain = new GameMain();
        gameMain.start();
    }

    public void start() {
        Graphics graphics = new Graphics();
        Thread graphicsThread = new Thread(graphics);
        graphicsThread.start();
    }
}
