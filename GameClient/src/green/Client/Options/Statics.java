package green.Client.Options;

import java.awt.*;

public class Statics {
    public static int port = 1111;
    public static String serverHost = "localhost";
    public static int gameSpeed = 100;
    public static String version = "0.0.2a";

    public static int height = 1000;
    public static int width = 1000;
    public static Color backgroundColor = Color.BLACK;

    public static int playerSize = 15;
    public static String playerName = version;
    public static int playerSpeed = 5;
    public static Color playerColor = Color.GREEN;

    public static int projectileSize = 10;
    public static int projectileSpeed = 2;

    public static int middleRespawnPosX = height / 2;
    public static int middleRespawnPosY = width / 2;

    public static int killTimeOut = 100;
}
