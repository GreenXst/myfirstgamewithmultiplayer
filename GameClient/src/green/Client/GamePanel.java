package green.Client;

import green.Client.Elements.EnemyManager;

import javax.swing.*;
import java.awt.Graphics;
import java.awt.*;

public class GamePanel extends JPanel {

    private EnemyManager enemyManager;

    GamePanel(EnemyManager enemyManager) {
        this.setBackground(Color.BLACK);
        this.enemyManager = enemyManager;
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);

        Graphics2D graphics2D = (Graphics2D) graphics;
        enemyManager.paint(graphics2D);
    }
}
