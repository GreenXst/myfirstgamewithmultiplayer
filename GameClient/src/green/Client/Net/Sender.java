package green.Client.Net;

import green.Client.Elements.Player;
import green.Client.Options.Statics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Sender {

    private Socket socket;
    private PrintWriter printWriter = null;
    private BufferedReader bufferedReader = null;

    public Sender() {
        try {
            socket = new Socket(Statics.serverHost, Statics.port);
            printWriter = new PrintWriter(socket.getOutputStream());
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            System.out.println("No Server Running!");
            System.exit(0);
        }
    }

    public void sendPlayerToServer(Player player) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(player.x).append(":").append(player.y).append(":").append(player.projectile.x).append(":")
                .append(player.projectile.y).append(":").append(player.name).append(":").append(player.id).append(":")
                .append(player.kills).append(":").append(player.color);
        printWriter.println(stringBuilder);
        printWriter.flush();
    }

    public String getServerReply(){
        String message = null;
        try {
            message = bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return message;
    }
}
