package green.Client;

import green.Client.Elements.EnemyManager;
import green.Client.Elements.Player;
import green.Client.Elements.Projectile;
import green.Client.Net.Sender;
import green.Client.Options.Statics;

import javax.swing.*;
import java.awt.*;

public class Graphics implements Runnable {
    @Override
    public void run() {
        boolean gameOn = true;
        JFrame game = new JFrame("The shot came from THERE! Multiplayer version " + Statics.version);
        Projectile projectile = new Projectile();
        EnemyManager enemyManager = new EnemyManager();
        Player player = new Player(projectile, (int) (System.currentTimeMillis() / 1000)); // The id = time, Method to generate unique id for any player
        GamePanel gamePanel = new GamePanel(enemyManager);
        GameKeyListener gameKeyListener = new GameKeyListener(player);
        Sender sender = new Sender();

        game.setBackground(Color.BLACK);
        game.setSize(Statics.width, Statics.height);
        game.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        game.add(gamePanel);
        game.addKeyListener(gameKeyListener);
        game.setVisible(true);

        // Prepare Player
        player.setRandomSpawnPosition();
        player.name = Statics.playerName;
        player.color = Statics.playerColor;

        // Prepare projectile
        projectile.setHiddenPosition();

        while (gameOn) {
            player.move();
            projectile.move();
            enemyManager.checkCollision(player);

            gamePanel.repaint();

            sender.sendPlayerToServer(player);
            enemyManager.createPlayersFromString(sender.getServerReply());

            try {
                Thread.sleep(Statics.gameSpeed);
            } catch (InterruptedException e) {
                e.printStackTrace();
                gameOn = false;
            }
        }
        System.exit(0);
    }
}
