package green.Client.Elements;

import green.Client.Options.Statics;

import java.awt.*;

public abstract class MovingObject {

    public int x = 0;
    public int y = 0;
    public int xa = 0;
    public int ya = 0;
    public int aliveTime = 0;

    public abstract void paint(Graphics2D graphics2D);

    public void move(){
        if (!(x < 0 || x > Statics.width - Statics.playerSize ||
                y < 0 || y > Statics.height - Statics.playerSize )){
            x += xa;
            y += ya;
        }
        aliveTime++;
    }

    public Rectangle getCollisionBox() {
        return new Rectangle(x, y, Statics.playerSize, Statics.playerSize);
    }
}
