package green.Client.Elements;

import green.Client.Options.Statics;

import java.awt.*;

public class Player extends MovingObject{

    public String name;
    public Projectile projectile;
    public int kills = 0;
    public int id;
    public Color color;
    private int lastKillTime = 0;

    public Player(Projectile projectile, int id) {
        this.projectile = projectile;
        this.id = id;
    }

    @Override
    public void paint(Graphics2D graphics2D) {
        graphics2D.setColor(color);
        graphics2D.drawOval(x, y, Statics.playerSize, Statics.playerSize);
        graphics2D.drawString(name + " | " + kills, x + (Statics.playerSize) * 2, y);
        projectile.paint(graphics2D);
    }

    public void shoot() {
        projectile.x = x;
        projectile.y = y;
        projectile.xa = xa * Statics.projectileSpeed;
        projectile.ya = ya * Statics.projectileSpeed;
    }

    void killed(Player playerI) {
        if (aliveTime - lastKillTime > Statics.killTimeOut) { //Duo the Server Client delay, we need a kill timeout!
            kills++;
            lastKillTime = aliveTime;
            System.out.println("You killed: " + playerI.name);
        }

    }

    void gotKilled(Player playerI) {
        setRandomSpawnPosition();
        System.out.println("Got killed by: " + playerI.name);
    }

    public void setRandomSpawnPosition() {
        x = (int) (Math.random() * Statics.middleRespawnPosX);
        y = (int) (Math.random() * Statics.middleRespawnPosY);
    }
}
