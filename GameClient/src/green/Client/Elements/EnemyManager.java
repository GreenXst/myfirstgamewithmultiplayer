package green.Client.Elements;

import java.awt.*;

public class EnemyManager {
    private Player[] players = new Player[0];

    public void createPlayersFromString(String serverReply) {
        if (serverReply.equals("")){
            System.out.println("Unknown Server response!");
            return;
        }

        String[] infos = serverReply.split(";");

        if (players.length != infos.length){ // Check if other players joined or left!
            players = new Player[infos.length];
            for (int i = 0; i < players.length; i++){
                Projectile projectile = new Projectile();
                players[i] = new Player(projectile, i);
            }
        }

        for (int i = 0; i < infos.length; i++){
            String[] playerinfos = infos[i].split(":");
            players[i].x = Integer.parseInt(playerinfos[0]);
            players[i].y = Integer.parseInt(playerinfos[1]);
            players[i].projectile.x = Integer.parseInt(playerinfos[2]);
            players[i].projectile.y = Integer.parseInt(playerinfos[3]);
            players[i].name = playerinfos[4];
            players[i].id = Integer.parseInt(playerinfos[5]);
            players[i].kills = Integer.parseInt(playerinfos[6]);
            players[i].color = Color.getColor(playerinfos[7]);
        }
    }

    public void paint(Graphics2D graphics2D){
        for (Player player : players){
            player.paint(graphics2D);
        }
    }

    public void checkCollision(Player player) {
        for (Player playerI : players) {
            if (playerI.projectile.getCollisionBox().intersects(player.getCollisionBox())
                    && player.id != playerI.id) {
                player.gotKilled(playerI);
            }

            if (playerI.getCollisionBox().intersects(player.projectile.getCollisionBox())
                    && player.id != playerI.id) { //Checks if localPlayer hits another Player;
                player.killed(playerI); //Need to check if he hits himself
            }
        }
    }
}
