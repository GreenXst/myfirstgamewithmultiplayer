package green.Client.Elements;

import green.Client.Options.Statics;

import java.awt.*;

public class Projectile extends MovingObject{


    @Override
    public void paint(Graphics2D graphics2D) {
        graphics2D.drawOval(x, y, Statics.projectileSize, Statics.projectileSize);
    }

    public void setHiddenPosition() {
        x = 0 - Statics.projectileSize;
        y = 0 - Statics.projectileSize;
    }
}
