package green.Client;

import green.Client.Elements.Player;
import green.Client.Options.Statics;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameKeyListener implements KeyListener {

    private Player target;

    GameKeyListener(Player player) {
        target = player;
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
        //Nothing to do, hopefully not executed!
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.VK_A){
            target.xa = -Statics.playerSpeed;
        }
        if (keyEvent.getKeyCode() == KeyEvent.VK_D) {
            target.xa = Statics.playerSpeed;
        }
        if (keyEvent.getKeyCode() == KeyEvent.VK_W) {
            target.ya = -Statics.playerSpeed;
        }
        if (keyEvent.getKeyCode() == KeyEvent.VK_S) {
            target.ya = Statics.playerSpeed;
        }
        if (keyEvent.getKeyCode() == KeyEvent.VK_SPACE) {
            target.shoot();
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.VK_A){
            target.xa = 0;
        }
        if (keyEvent.getKeyCode() == KeyEvent.VK_D) {
            target.xa = 0;
        }
        if (keyEvent.getKeyCode() == KeyEvent.VK_W) {
            target.ya = 0;
        }
        if (keyEvent.getKeyCode() == KeyEvent.VK_S) {
            target.ya = 0;
        }
    }
}
