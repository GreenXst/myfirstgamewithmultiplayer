package green.Server;

import green.Server.Options.Statics;

import java.util.List;

public class GameProcessor implements Runnable {

    ClientHandler clientHandler;

    public GameProcessor(ClientHandler clientHandler) {
        this.clientHandler = clientHandler;
    }

    @Override
    public void run() {
        List<ClientThread> clientThreadList = clientHandler.getClientThreadList();
        StringBuilder input;

        while (true) {
            input = new StringBuilder();
            int killMeId = -1;
            for (ClientThread clientThread : clientThreadList) { // Reads out all Threads and safes them in one big variable
                if (clientThread.killmeplease){
                    killMeId = clientThreadList.indexOf(clientThread);
                }
                input.append(clientThread.inputMessage).append(";");
            }
            for (ClientThread clientThread : clientThreadList){ // Stores the big variable into the outputMessage
                clientThread.outputMessage = input.toString();
            }

            if (killMeId > -1){
                clientThreadList.remove(killMeId);
            }

            try {
                Thread.sleep(Statics.processorSpeed);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
