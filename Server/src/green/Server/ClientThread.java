package green.Server;

import green.Server.Options.Statics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

public class ClientThread implements Runnable{

    Socket client;
    String inputMessage = "";
    String outputMessage = "";
    boolean killmeplease = false;

    public ClientThread(Socket socket) {
        client = socket;
    }

    @Override
    public void run() {
        System.out.println(client.getInetAddress() + " connected!");

        PrintWriter printWriter = null;
        BufferedReader bufferedReader = null;

        try {
            printWriter = new PrintWriter(client.getOutputStream());
            bufferedReader = new BufferedReader(new InputStreamReader(client.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert printWriter != null;
        assert bufferedReader != null;

        while(true){
            printWriter.println(outputMessage);
            printWriter.flush();
            try {
                inputMessage = bufferedReader.readLine();
                if (inputMessage == null){
                    bufferedReader.close();
                    throw new SocketException();
                }
                Thread.sleep(Statics.clientThreadSpeed);
            } catch (SocketException e) {
                System.out.println("Connection closed " + client.getInetAddress());
                printWriter.close();
                killmeplease = true;
                return;
            }catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
