package green.Server;

import java.util.Scanner;

public class GameServer {

    public static void main(String[] args){

        System.out.println("Starting Server!");

        GameServer gameServer = new GameServer();
        gameServer.start();

        //Make some commands available
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()){
            String input = scanner.nextLine().toLowerCase();
            String[] inputs = input.split(" ");

            if (input.equals("exit")){
                System.out.println("Exiting...");
                System.exit(0);
            } else {
                System.out.println("type help for help!");
            }
        }
    }

    public void start() {
        ClientHandler clientHandler = new ClientHandler();
        GameProcessor gameProcessor = new GameProcessor(clientHandler); //Client handler is needed for communication

        Thread processor = new Thread(gameProcessor);
        Thread thread = new Thread(clientHandler);

        processor.start();
        thread.start();
    }
}
