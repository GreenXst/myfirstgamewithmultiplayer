package green.Server;

import green.Server.Options.Statics;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/*
* This is my Client handler:
* it accepts new connection and pushes them into a new thread
*/

public class ClientHandler implements Runnable{

    List<ClientThread> clientThreadList = new ArrayList<>();

    @Override
    public void run() {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(Statics.port);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (true){
            try {
                assert serverSocket != null;
                Socket socket = serverSocket.accept();
                ClientThread clientThread = new ClientThread(socket);
                Thread thread = new Thread(clientThread);
                thread.start();
                clientThreadList.add(clientThread);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public List<ClientThread> getClientThreadList(){
        return clientThreadList;
    }
}
